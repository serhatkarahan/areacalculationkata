package com.serhatkarahan.areacalculation.calculator;

import com.serhatkarahan.areacalculation.shape.*;

public class Calculator {

    public double getTotalArea(Shape shape) {
        return shape.calculateTotalArea();
    }
}
