package com.serhatkarahan.areacalculation.shape;

public class Circle implements Shape {

    private double radiusLength;

    public double calculateTotalArea() {
        return Math.PI * radiusLength * radiusLength;
    }

    public double getradiusLength() {
        return radiusLength;
    }

    public void setradiusLength(double radiusLength) {
        this.radiusLength = radiusLength;
    }
}
