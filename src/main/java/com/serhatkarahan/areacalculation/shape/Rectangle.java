package com.serhatkarahan.areacalculation.shape;

public class Rectangle implements Shape {

    private double firstEdgeLength;
    private double secondEdgeLength;

    public double calculateTotalArea() {
        return firstEdgeLength * secondEdgeLength;
    }

    public double getFirstEdgeLength() {
        return firstEdgeLength;
    }

    public void setFirstEdgeLength(double firstEdgeLength) {
        this.firstEdgeLength = firstEdgeLength;
    }

    public double getSecondEdgeLength() {
        return secondEdgeLength;
    }

    public void setSecondEdgeLength(double secondEdgeLength) {
        this.secondEdgeLength = secondEdgeLength;
    }
}
