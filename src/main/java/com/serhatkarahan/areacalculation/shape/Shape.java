package com.serhatkarahan.areacalculation.shape;

public interface Shape {

    public double calculateTotalArea();
}
