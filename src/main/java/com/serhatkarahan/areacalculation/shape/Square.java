package com.serhatkarahan.areacalculation.shape;

public class Square implements Shape {

    private double edgeLength;

    public void setEdgeLength(double edgeLength) {
        this.edgeLength = edgeLength;
    }

    public double getEdgeLength() {
        return edgeLength;
    }

    public double calculateTotalArea() {
        return edgeLength * edgeLength;
    }
}
