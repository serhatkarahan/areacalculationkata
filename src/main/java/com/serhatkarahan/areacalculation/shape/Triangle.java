package com.serhatkarahan.areacalculation.shape;

public class Triangle implements Shape{

    private double baseLength;

    private double heightLength;

    public double calculateTotalArea() {
        return 0.5 * baseLength * heightLength;
    }

    public double getBaseLength() {
        return baseLength;
    }

    public void setBaseLength(double baseLength) {
        this.baseLength = baseLength;
    }

    public double getHeightLength() {
        return heightLength;
    }

    public void setHeightLength(double heightLength) {
        this.heightLength = heightLength;
    }

}
