import com.serhatkarahan.areacalculation.calculator.Calculator;
import com.serhatkarahan.areacalculation.shape.Rectangle;
import org.junit.*;
import com.serhatkarahan.areacalculation.shape.*;
import static org.assertj.core.api.Assertions.*;

public class CalculatorTest {

    Calculator calculator;
    Square square;
    Rectangle rectangle;
    Circle circle;
    Triangle triangle;

    @Before
    public void setUp() {
        calculator = new Calculator();
        square = new Square();
        rectangle = new Rectangle();
        circle = new Circle();
        triangle = new Triangle();
    }

    @Test
    public void should_return_16_when_given_square_with_edge_length_is_4() {
        //given
        square.setEdgeLength(4);

        //when
        double squareArea = calculator.getTotalArea(square);

        //then
        assertThat(squareArea).isEqualTo(16);
    }

    @Test
    public void should_return_49_when_given_square_edge_length_is_7() {
        //given
        square.setEdgeLength(7);

        //when
        double squareArea = calculator.getTotalArea(square);

        //then
        assertThat(squareArea).isEqualTo(49);
    }

    @Test
    public void should_return_24_given_rectangle_edge_lengths_are_3_and_8() {
        //given
        rectangle.setFirstEdgeLength(3);
        rectangle.setSecondEdgeLength(8);

        //when
        double rectangleArea = calculator.getTotalArea(rectangle);

        //then
        assertThat(rectangleArea).isEqualTo(24);
    }

    @Test
    public void should_return_31_dot_5_given_rectangle_edge_lengths_are_4_dot_5_and_7() {
        //given
        rectangle.setFirstEdgeLength(4.5);
        rectangle.setSecondEdgeLength(7);

        //when
        double rectangleArea = calculator.getTotalArea(rectangle);

        //then
        assertThat(rectangleArea).isEqualTo(31.5);
    }

    @Test
    public void should_return_16_pi_when_given_circle_with_radius_4() {
        //given
        circle.setradiusLength(4);

        //when
        double circleArea = calculator.getTotalArea(circle);

        //then
        assertThat(circleArea).isEqualTo(Math.PI * 16);
    }

    @Test
    public void should_return_11_dot_1598_given_triangle_with_edges_3_dot_6_6_dot_2_7_dot_15() {
        //given
        triangle.setBaseLength(3.6);
        triangle.setHeightLength(6.2);

        //when
        double triangleArea = calculator.getTotalArea(triangle);

        //then
        assertThat(triangleArea).isEqualTo(11.16);
    }
}
